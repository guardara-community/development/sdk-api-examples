#!/usr/bin/env python3

"""
The example shows how to create Message Templates with Variable Collections
assigned.

An API key with the `template:write` scope configured is required for the 
example to work.
"""

from guardara.internal.utils import Utils
from guardara.api import Session
from guardara.api.template.variable import Collection
from guardara.api.template.variable import Variable

def save_variable_collection(session, collection):
    """ Save the variable collection """
    (response_code, result) = session.connection.create(
        "template", "/collection",
        collection,
        headers={}
    )
    if response_code != 200:
        raise Exception("Failed to create variable collection")
    return result

session = Session(
    # Below we provide the Manager's address and port number directly as 
    # strings. Check out `api_list_messages_1.py` to see how this information
    # can be obtained via the SDK's utility methods.
    "127.0.0.1",
    8443,
    # If an API key was configured when setting up the API, it can be fetched
    # using the `get_api_key()` method of the Utils class.
    Utils.get_api_key(),
    cert_verify=False,
)
field = session.template.field

# Create Variable Collection and add the Username and Password variables.
# Finally, save the document.
variables = Collection("API Variables Example - Collection")
username = Variable("Username", "root", "User name", "parameter", False)
password = Variable("Password", "toor", "User password", "parameter", True)
variables.add(username).add(password)
variables = save_variable_collection(session, variables)

# Create a Message with the Variable Collection assigned
message = session.template.message.new("API Variables Example - Message")
message.set_variable_collection(variables.get("id"))

# Create two reference fields, one for the username, another for the password.
ref_username = field.reference("Username", "variable", username.get("id"))
ref_password = field.reference("Password", "variable", password.get("id"))

# We are going to separate the username and password with a colon within the
# Message Template
separator = field.delimiter("Space", ":")

# Add the fields to the Message
message.add(ref_username)
message.add(separator)
message.add(ref_password)

# Save the message.
message_id = message.save()

# Now if you look at the Test Assets page in GUARDARA Manager you should see
# the Variable Collection and the Message Template created. Open the Message
# Template and use the Preview feature to see how the data is rendered.
