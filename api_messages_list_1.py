#!/usr/bin/env python3

"""
This example demonstrates how to use the Python API to obtain the list of
Message templates.

An API key with the `template:read` scope assigned is required for the 
example to work.
"""

from guardara.internal.utils import Utils
from guardara.api import Session

# Below we use the utility methods to get the API address and the API key.
# If the SDK was not configured with this information, the Manager's address,
# port number and the API key can be provided directly as strings.
session = Session(
    Utils.get_manager_address(),
    Utils.get_manager_port(),
    # If an API key was configured when setting up the API, it can be fetched
    # using the `get_api_key()` method of the Utils class.
    Utils.get_api_key(),
    cert_verify=False,
)

messages = session.template.message.list()
print(messages)
