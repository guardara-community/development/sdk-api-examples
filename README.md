# GUARDARA API Examples

This repository is a collection of Python scripts demonstrating the use of GUARDARA's Python SDK to work with the API. Make sure to check out the documentation to learn more about how to use the SDK.

## Examples

| Example      | Description | API Scopes | File     |
| ------------ | ----------- | ---------- | -------- |
| List Engines | The example demonstrates how to use the Python API to obtain the list of registered Engines. | `engine:read` | [api_engine_list.py](./api_engine_list.py) |
| Get Engine Status | This example demonstrates how to use the Python API to obtain the list of registered Engines and fetch the status of one of the Engines. | `engine:read`, `enginemanager:read` | [api_engine_status.py](./api_engine_status.py) |
| List Messages | This example demonstrates how to use the Python API to obtain the list of Message templates. | `template:read` | [api_messages_list_1.py](./api_messages_list_1.py) |
| List Messages (Low-level) | This example demonstrates how to use the Python API to obtain the list of Message templates. The example uses the low-level `fetch()` operation to fetch the data from the Template service. When using the low-level API we receive the actual template data as well. | `template_read` | [api_messages_list_2.py](./api_messages_list_2.py) |
| Create Message Template | The example shows how to create Message templates using the SDK. In this example we create a Message template that by default renders "Hello World". | `template:write` | [api_message_create.py](./api_message_create.py) |
| Using Session Variables | The example shows how to use a Variable Collection and Session Variables. The example also implements a Message Template that uses the Reference Field to utilize the Session Variables created. | `template:write` | [api_message_with_variables.py](./api_message_with_variables.py) |
| Message Preview | The example creates a Message template that by default renders "Hello World". Then, it uses the Preview feature to display the data we have modelled as it would be rendered by the Engine. | `template:write`, `enginemanager:write` | [api_message_preview.py](./api_message_preview.py) |
| Using Transforms | The example demonstrate how to create a Message template that utilizes Transforms to encode parts of the data modelled in different ways. | `template:write`, `enginemanager:write` | [api_message_transforms.py](./api_message_transforms.py) |
