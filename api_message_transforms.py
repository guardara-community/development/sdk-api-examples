#!/usr/bin/env python3

"""
In this example we create a Message template that utilizes Transforms to encode
parts of the data modelled in different ways.

An API key with the `template:write` and `enginemanager:write` scopes assigned
is required for the example to work.
"""

import base64
import binascii
from guardara.internal.utils import Utils
from guardara.api import Session

session = Session(
    # Below we provide the Manager's address and port number directly as 
    # strings. Check out `api_list_messages_1.py` to see how this information
    # can be obtained via the SDK's utility methods.
    "127.0.0.1",
    8443,
    # If an API key was configured when setting up the API, it can be fetched
    # using the `get_api_key()` method of the Utils class.
    Utils.get_api_key(),
    cert_verify=False,
)

field = session.template.field
transform = session.template.transform

# Create a Message template with the name "Transform Example"
message = session.template.message.new("Transform Example")

# Create an empty group called "Example"
group = field.group("Example")

# Create a field and apply Base64 transformation to it.
string = field.string("Greeting", "Hi there!")
string.transform(transform.base64())

# Add the field to the group as a group must have at least
# one children.
group.add(string)

# Add a transform to the newly created group too, just for fun.
group.transform(transform.hexstring())

# Add the fields to the Message
message.add(group)

# Call the preview to see the raw rendered data
print("-" * 80)
preview_data = message.preview()
print("PREVIEW: ", preview_data)
# Remove the applied hex encoding transform
preview_data_hex_decoded = binascii.unhexlify(preview_data)
print("HEX DECODED: ", preview_data_hex_decoded, " (Still Base64 encoded)")
# Remove the applied Base64 transform. We should get our original string
# "Hi there!"
print("BASE64 DECODED: ", base64.b64decode(preview_data_hex_decoded))

# Call the preview with `index` set to greater than zero to see the 48th
# mutation as it would be produced during a test run
print("-" * 80)
preview_data = message.preview(index=48)
print("MUTATED PREVIEW: ", preview_data)
# Remove the applied hex encoding transform
preview_data_hex_decoded = binascii.unhexlify(preview_data)
print("HEX DECODED: ", preview_data_hex_decoded, " (Still Base64 encoded)")
# Remove the applied Base64 transform. We should get a mutated value
print("BASE64 DECODED: ", base64.b64decode(preview_data_hex_decoded))
