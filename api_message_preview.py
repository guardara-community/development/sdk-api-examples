#!/usr/bin/env python3

"""
In this example we create a Message template that by default renders 
"Hello World" and use the Preview feature to inspect the data we have 
modelled as it would be rendered by the Engine.

An API key with the `template:write` and `enginemanager:write` scopes assigned
is required for the example to work.
"""

from guardara.internal.utils import Utils
from guardara.api import Session

session = Session(
    # Below we provide the Manager's address and port number directly as 
    # strings. Check out `api_list_messages_1.py` to see how this information
    # can be obtained via the SDK's utility methods.
    "127.0.0.1",
    8443,
    # If an API key was configured when setting up the API, it can be fetched
    # using the `get_api_key()` method of the Utils class.
    Utils.get_api_key(),
    cert_verify=False,
)

field = session.template.field

# Create a Message with the name "Hello World"
message = session.template.message.new("Hello World")

# Create the fields
p_first = field.string("FirstWord", "Hello")
p_space = field.delimiter("Space", " ")
p_second = field.string("SecondWord", "World")

# Add the fields to the Message
message.add(p_first)
message.add(p_space)
message.add(p_second)

# Call the preview to see the raw rendered data
print(message.preview())

# Call the preview with `index` set to greater than zero, in this case,
# `542` to see the 542nd test case as it would be produced during a test run.
print(message.preview(index=542))

# The same as above but HEX encoded
print(message.preview(format="hex", index=542))
