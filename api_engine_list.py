#!/usr/bin/env python3

"""
This example demonstrates how to use the Python API to obtain the list of
registered Engines.

An API key with the `engine:read` scope assigned is required for the example
to work.
"""

from guardara.internal.utils import Utils
from guardara.api import Session

session = Session(
    # Below we provide the Manager's address and port number directly as 
    # strings. Check out `api_list_messages_1.py` to see how this information
    # can be obtained via the SDK's utility methods.
    "127.0.0.1",
    8443,
    # If an API key was configured when setting up the API, it can be fetched
    # using the `get_api_key()` method of the Utils class.
    Utils.get_api_key(),
    cert_verify=False,
)

engines = session.engines.list()
print(engines)
